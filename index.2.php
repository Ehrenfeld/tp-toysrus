<?php 
    require_once 'app/controllers/config.php'; 
    require_once 'app/php/brand-affichage.php';
?>

<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>ToysRus</title>

    <?php echo $link; ?>

</head>
<body>
    <div class="principal-div">

        <!-- LOGO -->
        <a href="index.php"><img src="app/media/logo.png" id="logo"></a> 

        <!-- Menu -->
        <div class="menu">
            <nav class="btn-menu">
                <ul>
                    <li class="btn1">
                        <a href="all_article.php">Tous les jouets</a>
                    </li>
                    <li class="btn2">Par marque <i class="fas fa-caret-down"></i>
                        <ul>
                            <?php echo brand_list();?>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    
        <!-- Produit & Titre page-->
        <?php 
            echo affichage_toy(); 
        ?>
    </div>
</body>
</html>