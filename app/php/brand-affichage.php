<?php

/**
 * Affiche les marques dans le menu :hover (menu bleu).
 */
function brand_list()
{
    $html = '';
    global $mysql;

    $query = sprintf('SELECT brands.*, SUM(toys.brand_id = brands.id) AS total
                    FROM toys
                    JOIN brands ON toys.brand_id = brands.id
                    GROUP BY brands.id
                    ORDER BY brands.id');

    if ($result = mysqli_query($mysql, $query))
    {
        while ($row = mysqli_fetch_assoc($result))
        {

            $html .= sprintf('<li class="btn-marq">
                                <a href="http://tp-projet-web.net/index.1.php?brand=%s">%s (%s)</a>
                            </li>', $row['id'], $row['name'], $row['total']);
        }
        return $html;
    }
}

/**
 * Affiche les marques dans les menus "select".
 */
function select_brand()
{
    $html = '';
    global $mysql;

    $query = sprintf('SELECT `name`, brands.`id` FROM brands');

    if ($result = mysqli_query($mysql, $query))
    {
        while ($row = mysqli_fetch_assoc($result))
        {

            if ($_GET['brand'] == $row['id'])
            {
                $html .= sprintf('
                        <option selected="selected" value="%s">%s</option>', $row['id'], $row['name']);
            }

            $html .= sprintf('
                    <option value="%s">%s</option>', $row['id'], $row['name']);
        }
        return $html;
    }
}

/**
 * Affiche les jouets selon la marque choisi avec brand_list() ou select_brand().
 */
function brand()
{
    if ($_SERVER['REQUEST_METHOD'] === 'GET')
    {

        $html = '';
        global $mysql;

        $req = 'SELECT toys.* FROM toys 
                JOIN brands 
                ON brands.id = toys.brand_id 
                WHERE brand_id = ?';

        if ($stmt = mysqli_prepare($mysql, $req))
        {

            mysqli_stmt_bind_param($stmt, 's', $_GET['brand']);

            mysqli_stmt_execute($stmt);

            $stmt = mysqli_stmt_get_result($stmt);

            while ($row = mysqli_fetch_assoc($stmt))
            {

                $html .= sprintf(' 
                <div class="prod">
                    <a href="index.2.php?id=%s">
                        <img src="app/media/%s" alt="">
                        <p class="desc-prod">%s</p>
                        <span class="price">%s $</span>
                    </a>
                </div>', $row['id'], $row['image'], $row['name'], $row['price']);
            }
            return $html;
        }
    }
}

/**
 * Affiche les jouets: soit tout les toys si $_REQUEST est false.
 * et les toys par marque si $_REQUEST['brand'] est true.
 */
function all_toy()
{
    if ($_REQUEST)
    {
        echo brand();
    }

    else
    {
        echo toys_list();
    }
}

/**
 * Switch de condition entre brand() et toys_list_ID.
 */
function toys()
{

    if ($_SERVER['REQUEST_METHOD'] === 'GET')
    {
        echo brand();
    }

    else
    {
        echo toys_list_ID();
    }
}

/**
 * Affiche les jouets selon la marque choisi.
 */
function toys_list_GET()
{
    $html = '';
    global $mysql;

    $req = 'SELECT toys.* FROM toys 
                JOIN brands 
                ON brands.id = toys.brand_id 
                WHERE brand_id = ?';

    if ($stmt = mysqli_prepare($mysql, $req))
    {

        mysqli_stmt_bind_param($stmt, 's', $_GET['id']);

        mysqli_stmt_execute($stmt);

        $stmt = mysqli_stmt_get_result($stmt);

        while ($row = mysqli_fetch_assoc($stmt))
        {

            $html .= sprintf(' 
                <div class="prod">
                    <a href="index.2.php?id=%s">
                        <img src="app/media/%s" alt="">
                        <p class="desc-prod">%s</p>
                        <span class="price">%s $</span>
                    </a>
                </div>', $row['id'], $row['image'], $row['name'], $row['price']);
        }
        return $html;
    }
}

/**
 * Affiche les jouets selon la marque choisi.
 */
function toys_list_ID()
{

    $html = '';
    global $mysql;

    $req = 'SELECT toys.* FROM toys 
                JOIN brands 
                ON brands.id = toys.brand_id 
                WHERE brand_id = ?';

    if ($stmt = mysqli_prepare($mysql, $req))
    {

        mysqli_stmt_bind_param($stmt, 's', $_GET['brand']);

        mysqli_stmt_execute($stmt);

        $stmt = mysqli_stmt_get_result($stmt);

        while ($row = mysqli_fetch_assoc($stmt))
        {

            $html .= sprintf(' 
                <div class="prod">
                    <a href="index.2.php?id=%s">
                        <img src="app/media/%s" alt="">
                        <p class="desc-prod">%s</p>
                        <span class="price">%s $</span>
                    </a>
                </div>', $row['id'], $row['image'], $row['name'], $row['price']);
        }
        return $html;
    }
}

/**
 * Affiche les jouets dans le menu Tous les jouets soit le fichier all_article.php.
 */
function toys_list()
{

    $html = '';
    global $mysql;

    $query = sprintf('SELECT * FROM toys');

    if ($result = mysqli_query($mysql, $query))
    {
        while ($row = mysqli_fetch_assoc($result))
        {

            $html .= sprintf(' 
            <div class="prod">
                <a href="index.2.php?id=%s">
                    <img src="app/media/%s" alt="">
                    <p class="desc-prod">%s</p>
                    <span class="price">%s $</span>
                </a>
            </div>', $row['id'], $row['image'], $row['name'], $row['price']);
        }
        return $html;
    }

}

/**
 *  Affiche les 3 jouets les mieux vendu.
 */
function best_sale()
{
    $html = '';
    global $mysql;

    $query = sprintf('SELECT toys.*, SUM(`quantity`) AS total
                    FROM `toys`
                    JOIN `sales` ON toys.id = sales.toy_id
                    GROUP BY toys.`id` ORDER BY total DESC LIMIT 0,3;');

    if ($result = mysqli_query($mysql, $query))
    {
        while ($row = mysqli_fetch_assoc($result))
        {

            $html .= sprintf(' 
            <div class="prod">
                <a href="index.2.php?id=%s">
                    <img src="app/media/%s" alt="">
                    <p class="desc-prod">%s</p>
                    <span class="price">%s $</span>
                </a>
            </div>', $row['id'], $row['image'], $row['name'], $row['price']);
        }
        return $html;
    }
}

//=====//===== Page de détail d'un jouet =====//=====//

/**
 * Affiche la page de détail d'un jouet avec l'aide des function:
 *  - brand_name()
 *  - affichage_toy_name()
 *  - affichage_toy_store()
 *  - affichage_toy_stock()
 */
function affichage_toy()
{
    $html = '';
    global $mysql;

    $req = 'SELECT * FROM toys WHERE toys.id = ?';

    if ($stmt = mysqli_prepare($mysql, $req))
    {

        mysqli_stmt_bind_param($stmt, 's', $_GET['id']);

        mysqli_stmt_execute($stmt);

        $stmt = mysqli_stmt_get_result($stmt);

        while ($row = mysqli_fetch_assoc($stmt))
        {

            $html .= affichage_toy_name();

            $html .= sprintf('<div class="left-centre">
                <img src="app/media/%s" alt="">', $row['image']);

            $html .= sprintf('<div class="left-prod">    
                <span class="price-prod">%s $</span>', $row['price']);

            $html .= sprintf('
                <form method="GET">
                    <select name="stock">
                    <option>Quelle magasin ?</option>' 
                    . affichage_toy_store() . 
                    '</select> <input type="hidden" value="%s" name="id"> 
                    <button type="submit">OK</button><br>
                </form>' . affichage_toy_stock() , $row['id']);

            $html .= sprintf(' 
                </div>
            </div>
            <div class="right-prod">' . brand_name() . '<div class="description"> %s </div>
            </div>', $row['description']);
        }
        return $html;
    }
}

/**
 * Affiche le nom de la marque du jouet.
 */
function brand_name()
{

    $html = '';
    global $mysql;

    $req = sprintf('SELECT brands.name FROM brands JOIN toys ON brands.id = brand_id WHERE toys.id = ?');

    if ($stmt = mysqli_prepare($mysql, $req))
    {

        mysqli_stmt_bind_param($stmt, 's', $_GET['id']);

        mysqli_stmt_execute($stmt);

        $stmt = mysqli_stmt_get_result($stmt);

        while ($row = mysqli_fetch_assoc($stmt))
        {

            $html .= sprintf(' 
            <span class="marque"><span class="marque-color">Marque: </span>%s</span>', $row['name']);
        }
        return $html;
    }

}

/**
 * Affiche le nom du jouet.
 */
function affichage_toy_name()
{

    $html = '';
    global $mysql;

    $req = 'SELECT * FROM toys WHERE id = ?';

    if ($stmt = mysqli_prepare($mysql, $req))
    {

        mysqli_stmt_bind_param($stmt, 's', $_GET['id']);

        mysqli_stmt_execute($stmt);

        $stmt = mysqli_stmt_get_result($stmt);

        while ($row = mysqli_fetch_assoc($stmt))
        {

            $html .= sprintf('<div class="header-prod">%s</div>
                <div class="div-prod">', $row['name']);

        }
        return $html;
    }
}

/**
 * Affiche la liste des stores.
 */
function affichage_toy_store()
{

    ini_set("display_errors", 0);
    error_reporting(0);
    $html = '';
    global $mysql;

    $query = sprintf('SELECT * FROM stores');

    if ($result = mysqli_query($mysql, $query))
    {
        while ($row = mysqli_fetch_assoc($result))
        {

            if ($_GET['stock'] === $row['id'])
            {
                $html .= sprintf('
                        <option selected="selected" value="%s">%s</option>', $row['id'], $row['name']);
                continue;
            }

            $html .= sprintf('
            <option value="%s">%s</option>', $row['id'], $row['name']);
        }
    }

    return $html;
}

/**
 * Affiche le stock du jouet dans tous les stores OU dans le magasin choisi dans affichage_toy_store().
 */
function affichage_toy_stock()
{

    if (empty($_GET['stock']))
    {
        global $html;
        global $mysql;
        global $id_toy;

        $req = sprintf('SELECT *, SUM(`quantity`) AS total
                            FROM stock
                            WHERE toy_id = ?');

        if ($stmt = mysqli_prepare($mysql, $req))
        {

            mysqli_stmt_bind_param($stmt, 's', $_GET['id']);

            mysqli_stmt_execute($stmt);

            $stmt = mysqli_stmt_get_result($stmt);

            while ($row = mysqli_fetch_assoc($stmt))
            {

                $html .= sprintf('<span class="stock"><span class="stock-color">
                Stock: </span>%s</span>', $row['total']);
            }
            return $html;
        }
    }

    else
    {

        global $html;
        global $mysql;

        $req = sprintf('SELECT *, SUM(`quantity`) AS total
                            FROM stock
                            WHERE toy_id = ?
                            AND store_id = ?');

        if ($stmt = mysqli_prepare($mysql, $req))
        {

            mysqli_stmt_bind_param($stmt, 'ss', $_GET['id'], $_GET['stock']);

            mysqli_stmt_execute($stmt);

            $stmt = mysqli_stmt_get_result($stmt);

            while ($row = mysqli_fetch_assoc($stmt))
            {

                $html .= sprintf('<span class="stock">
                <span class="stock-color">
                Stock: </span>%s</span>', $row['total']);
            }
            return $html;

        }
    }

}
