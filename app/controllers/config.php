<?php

/**
 * Define des "log" pour accéder.
 */
define( 'DB_HOST', 'localhost' );
define( 'DB_USER', 'root' );
define( 'DB_PASS', 'root' );
define( 'DB_NAME', 'toys-r-us');

// Ouverture de la connection à la base de données.
$mysql = mysqli_connect( DB_HOST, DB_USER, DB_PASS, DB_NAME );

/**
 * Tous les liens:
 * - Google Fonts (Open Sans, 400 et 700 )
 * - FontAwsome
 * - Style.css
 */
$link = '<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
         <link rel="stylesheet" href="app/css/style.css">
         <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">';
